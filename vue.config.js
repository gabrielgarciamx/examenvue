const path = require('path');
const CopyPlugin = require('copy-webpack-plugin');
console.log("vue.config");
function resolveSrc(_path) {
  return path.join(__dirname, _path)
}
module.exports = {
  
  chainWebpack: config => {
    config.resolve.alias
      .set('TweenMax', 'gsap/src/uncompressed/TweenMax.js')
      .set('TimelineMax', 'gsap/src/uncompressed/TimelineMax.js')
      .set('ScrollToPlugin', 'gsap/src/uncompressed/plugins/ScrollToPlugin.js')
      .set('ScrollMagic', 'scrollmagic/scrollmagic/uncompressed/ScrollMagic.js')
      .set('animation.gsap', 'scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap.js')
      .set('debug.addIndicators', 'scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators.js')
  },
  publicPath: '/',
  outputDir: path.resolve(__dirname, "./dist"),
  indexPath: path.resolve(__dirname, "./dist/index.html"),
  // assetsDir: '/',
  // outputDir: path.resolve(__dirname, "../wwwroot/distro/resources"),
  // indexPath: path.resolve(__dirname, "../wwwroot/webex/index.jsp"),
 
assetsDir:'./',
  filenameHashing: true,
  productionSourceMap : false,
  configureWebpack: {
    // Set up all the aliases we use in our app.
    resolve: {
      alias: {
        'src': resolveSrc('src'),
        'assets': resolveSrc('src/assets')
      }
    }
  },
  css: {
    // Enable CSS source maps.
    sourceMap: true,
    loaderOptions: {
      sass: {
        data: `
          @import "vue-select/src/scss/vue-select.scss";
          @import '~vue-range-slider/dist/vue-range-slider.scss';
          @import "~@/sass/base/variables.scss";
          @import "~@/sass/base/sizes.scss";
          @import "~@/sass/base/atomos.scss";
          @import "~@/sass/base/molecules.scss";
          @import "~@/sass/base/customElements.scss";
          @import url("//unpkg.com/element-ui@2.13.0/lib/theme-chalk/index.css");
        `
      },
    },
  },
  pwa: {
    themeColor: '#FFA534',
    manifestPath: process.env.PATH_MANIFEST
  },
};

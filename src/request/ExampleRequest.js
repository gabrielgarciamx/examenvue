import axios from 'axios'
import Constants from '../library/ConstantsLibrary.js';
import qs from 'qs';

export default {
	async getBranches() {
		let data = {
			action: "r",
			name: "",
			disabled: 0,
			version: 1,
			simpleRequest: 0
		};
		return axios.post('/search/catalog/branch',data);
	},
	async getBrands() {
		let data = {
			action: "r",
			name: "",
			disabled: 0,
			version: 1,
			simpleRequest: 0
		};
		return axios.post('/cloud-kitchen/brand/catalog',data);
	},
	async getDeliveries() {
		let data = {
			action: "r"
		};
		return axios.post('/search/business/delivery/conf',data);
	},
}
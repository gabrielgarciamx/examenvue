import axios from 'axios'
import qs from 'qs';

export default {
    async sendMail(action, formName, formId, data) {
        if (!action) action = "saveForm";
        if (!formName) formName = "contact";
        if (!formId) formId = "1";
        let formData = new FormData();
        let keys = Object.keys(data);
        formData.append("action", action);
        formData.append("form", formName);
        formData.append("gonet_id_form", formId);
        formData.append("gonet_active", "1");
        formData.append("gonet_record", "gonet_record");
        formData.append("gonet_url", "https://gonetpos.com");
        keys.forEach((key) => {
            if (data[key].hasOwnProperty('type') && data[key].type == "file") {
                formData.append("gonet_file", data[key].file);
                formData.append("gonet_file_base ", data[key].base);
                formData.append("gonet_file_data", data[key].data);
            } else {
                formData.append("gonet_answer_" + key, data[key]);
            }
        });
        // let toSend = JSON.stringify(data);
        return axios.post('/apiHandler.php', formData, {
            headers: {
                "content-type": "multipart/form-data"
            }
        });
    },
}
import { HorizontalBar, mixins } from "vue-chartjs";
export default {
  extends: HorizontalBar,
  props: ["data", "options"],
  
  methods: {
    update() {
      this.$data._chart.update()
    }
  },
  mounted() {
    this.renderChart(this.data,this.options, {
      borderWidth: "10px",
      hoverBackgroundColor: "red",
      hoverBorderWidth: "10px"
    }); 
  },
  watch: {
    data: function(newVal, oldVal) {
      this.renderChart(newVal,this.options, {
        borderWidth: "10px",
        hoverBackgroundColor: "red",
        hoverBorderWidth: "10px"
      });
    }
  }
};

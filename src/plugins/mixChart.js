import { Bar, mixins } from "vue-chartjs";
export default {
  extends: Bar,
  props: ["data", "options"],
  
  methods: {
    update() {
      this.$data._chart.update()
    }
  },
  mounted() {
    this.renderChart(this.data,this.options, {
      borderWidth: "10px",
      hoverBackgroundColor: "red",
      hoverBorderWidth: "10px"
    });
    
  },
  watch: {
    data: function(newVal, oldVal) {
      // eslint-disable-next-line no-console
      console.log('Prop changed: ', newVal, ' | was: ', oldVal)
      this.renderChart(newVal,this.options, {
        borderWidth: "10px",
        hoverBackgroundColor: "red",
        hoverBorderWidth: "10px"
      });
    }
  }
};

import Vue from 'vue';
import VueI18n from 'vue-i18n';

import store from '../library/store/store.js';
// import elementEn from 'element-ui/lib/locale/lang/en';
// import elementEs from 'element-ui/lib/locale/lang/es';
import Constants from '../library/ConstantsLibrary.js';

import common from './ModulesTranslation/Common.js';
import interfaceTranslate from './ModulesTranslation/Interface.js';
import header from './ModulesTranslation/menuSite.js';

/*Children*/
import aboutUs from './ModulesTranslation/aboutUs.js';
import contact from './ModulesTranslation/contact.js';
import home from './ModulesTranslation/home.js';
import buttomsbottom from './ModulesTranslation/buttomsbottom.js';
import footer from './ModulesTranslation/footer.js';
import example from './ModulesTranslation/Example.js';
import date from './ModulesTranslation/Date.js';

Vue.use(VueI18n);

const messages = {
    'en': {
        languages: 'languages',
        titleHome: 'Home',
        titleInterface: 'Documentation',
        titleIcon: 'Icons',
        titleAtoms: 'Atoms',

        /* Interface */
        interface: interfaceTranslate.en,
        /* Commons text */
        common: common.en,
        date: date.en,

        aboutUs: aboutUs.en,

        header: header.en,
        buttomsbottom: buttomsbottom.en,
        footer: footer.en,
        home: home.en

    },
    'es': {
        languages: 'Idiomas',
        titleHome: 'Inicio',
        titleInterface: 'Documentación',
        titleIcon: 'Íconos',
        titleAtoms: 'Átomos',
        /*Menu start*/
        orders: 'Órdenes',
        history: 'Historial',
        deliveries: 'Deliveries',
        help: 'Ayuda',
        /*Menu end*/
        logOut: 'Cerrar sesión',
        /* Element UI */
        // el: elementEs.el,
        /* Interface */
        interface: interfaceTranslate.es,
        /* Commons text */
        common: common.es,
        date: date.es,

        aboutUs: aboutUs.es,

        contact: contact.es,

        example: example.es,
        header: header.es,
        buttomsbottom: buttomsbottom.es,
        footer: footer.es,
        home: home.es,

    }

}

let language = store.getters["translate/getLanguaje"];
const i18n = new VueI18n({
    locale: "en", // set locale
    fallbackLocale: Constants.defaultFallbackLocale, // set fallback locale
    messages, // set locale messages
});

export default i18n;
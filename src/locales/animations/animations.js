
export default {



    downUp(element, tiempo) {
        let animation = TweenMax.fromTo(element, tiempo, { y: 200, opacity: 0 }, { y: 0, opacity: 1 })
        return animation
    },

    downUpElements(element, cant) {
        let animation = []
        for (let i = 0; i <= cant; i++) {
            animation[i] = TweenMax.fromTo(element + i, [i + 1] * 0.2, { y: 200, opacity: 0 }, { y: 0, opacity: 1 })
        }

        return animation
    },

    leftRightElements(element, cant) {
        let animation = []
        for (let i = 0; i <= cant; i++) {
            animation[i] = TweenMax.fromTo(element + i, [i + 1] * 0.2, { x: 200, opacity: 0 }, { x: 0, opacity: 1 })
        }

        return animation
    },

    scaleElements(element, cant) {
        let animation = []
        for (let i = 0; i <= cant; i++) {
            animation[i] = TweenMax.fromTo(element + i, [i + 1] * 0.2, { scale: 0, opacity: 0 }, { scale: 1, opacity: 1 })
        }

        return animation
    },

    scaleElement(element,tiempo) {
         let animation = TweenMax.fromTo(element, tiempo, { scale: 0, opacity: 0 }, { scale: 1, opacity: 1 })

        return animation
    }
}
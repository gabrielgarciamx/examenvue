export default{
    'en':{
        at: '{day} {month} at {hours}:{minutes}:{seconds}',
        months: {
            m1: "January",
            m2: "February",
            m3: "March",
            m4: "April",
            m5: "May",
            m6: "June",
            m7: "July",
            m8: "August",
            m9: "September",
            m10: "October",
            m11: "November",
            m12: "December",
        }
    },
    'es':{
        at: '{day} {month} a las {hours}:{minutes}:{seconds}',
        months: {
            m1: "Enero",
            m2: "Febrero",
            m3: "Marzo",
            m4: "Abril",
            m5: "Mayo",
            m6: "Junio",
            m7: "Julio",
            m8: "Agosto",
            m9: "Septiembre",
            m10: "Octubre",
            m11: "Noviembre",
            m12: "Diciembre",
        }
    }
}

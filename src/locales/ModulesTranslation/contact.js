export default {
    "en": {
        "meta": {
            "lang": "en-US",
            "locate": "en_US",
            "title": "",
            "description": "",
        },
        "info": {
            "title": "",
            "legend": "",
        },

        "bg": {
            "principal": require('@/assets/images/contact/principal-min.jpg'),
            "rocket": require('@/assets/images/contact/rocket.jpg'),
        },
    }
}
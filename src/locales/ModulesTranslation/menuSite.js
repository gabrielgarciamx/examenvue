export default {
    'en': {
        menu1: '',
        menu2: 'About us',
        menu3: 'Sign Up',
        menu4: '',
        menu5: '',
        submenu1: '',
        submenu2: '',
        submenu3: '',
        btn: 'Sign In',
    },
    'es': {
        menu1: '',
        menu2: 'Nosotros',
        menu3: 'Registro',
        menu4: '',
        menu5: '',
        submenu1: '',
        submenu2: '',
        submenu3: '',
        btn: 'Acceder',
    }
}
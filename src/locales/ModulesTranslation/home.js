export default {
    "en": {
        "meta": {
            "lang": "en-US",
            "locate": "en_US",
            "title": "gonet - People are the way",
            "description": "gonet is an international company specialized in provide software development and technology solutions according to the needs of the industry",
        },
        "info": {
            "title": "",
            "legend": "",
        },

        "bg": {
            background1: require('@/assets/images/about/bg-experience.jpg'),
            background2: require('@/assets/images/about/bg-strategy.jpg'),
            background3: require('@/assets/images/about/bg-growth.jpg'),
        },

    },
    "es": {
        "meta": {
            "lang": "es-MX",
            "locate": "es_MX",
            "title": "gonet - People are the way",
            "description": "gonet es una compañía internacional especializada en desarrollo de software y tecnoloogía para brindar soluciones a diferentes industrias.",
        },
        "info": {
            "title": "About us",
            "legend": "gonet was founded in 2008 with the main goal of helping several industries to innovate themselves through technology. We have worked to provide the best solutions and found talented people from all over the world are the main key to achieve this, we work according to and confirmed that People are the Way.",
        },

        "bg": {
            background1: require('@/assets/images/about/bg-experience.jpg'),
            background2: require('@/assets/images/about/bg-strategy.jpg'),
            background3: require('@/assets/images/about/bg-growth.jpg'),
        },
    }
}
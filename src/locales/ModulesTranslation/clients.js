export default {

    "en": {
        "meta": {
            "lang": "en-US",
            "locate": "en_US",
            "title": "Our clients",
            "description": "We are proud of our achievements whithin some industries like Aeronautics, Retail, Banking, Fintech and more",
        },
        "video": {
            "path": "https://player.vimeo.com/external/548053690.hd.mp4?s=efe11081b46260c59b2fd14507bc8ddc2b56b9c3&profile_id=175",
            "icon": "https://image.flaticon.com/icons/png/512/117/117999.png",
            "title": "Clients",
            "legend": "",
        },
        "image": {

        },
        "icons": {

        },
        "message": {
            "info": {
                "mainTilte": "What can we do for your business?",
                "mainText": "Discover how gonet is working as a partner for several businesses to help them grow within their industry."
            },
            "clients": {
                "client1": {
                    "description": "gonet works every day to maintain security within the company working with our cybersecurity teams and app development.",
                },
                "client2": {
                    "description": "Nowadays, more than 111 gonet certified professionals work in different platforms and areas of the company for more than 10 years.",
                },
                "client3": {
                    "description": "We developed several apps for BBVA and have been working with them for over 10 years.",
                },
                "client4": {
                    "description": "Creation of integral system that made their operation better and facilitated their management.",
                },
                "client5": {
                    "description": "Development of the Starbucks Rewards e-commerce platform from several countries, like Chile, Argentina, and Colombia.",
                },
                "client6": {
                    "description": "gonet works every day to maintain security within the company working with our cybersecurity teams and app development.",
                },
                "btnView": "View client",
            },
            "section2": {
                "title": "Success cases",
                "legend1": "We are proud of every achievement we have accomplished together with our clients.",
            },

        },
    },
    "es": {
        "meta": {
            "lang": "es-MX",
            "locate": "es_MX",
            "title": "Clientes",
            "description": "Hemos trabajado y logrado una diferencia en algunas industrias con compañías importantes en su campo.",
        },
        "video": {
            "path": "https://player.vimeo.com/external/548053690.hd.mp4?s=efe11081b46260c59b2fd14507bc8ddc2b56b9c3&profile_id=175",
            "icon": "https://image.flaticon.com/icons/png/512/117/117999.png",
            "title": "Clients",
            "legend": "",
        },
        "image": {

        },
        "icons": {

        },
        "message": {
            "info": {
                "mainTilte": "¿Qué podemos hacer por tu negocio?",
                "mainText": "Descubre lo que gonet hace para ayudar a nuestros clientes a crecer dentro de la industria."
            },
            "clients": {
                "client1": {
                    "description": "gonet trabaja todos los días para mantener la seguridad dentro de la compañía con nuestros equipos de ciberseguridad y desarrollo de aplicaciones.",
                },
                "client2": {
                    "description": "Actualmente tenemos más de 110 profesionales certificados en diferentes plataformas y trabajando para distintas áreas de la compañía desde hace más de 10 años.",
                },
                "client3": {
                    "description": "Desarrollamos diferentes aplicaciones para BBVA y forjado una relación con ellos a lo largo de diez años.",
                },
                "client4": {
                    "description": "Creamos un sistema integral que facilitó y mejoró su operación y administración.",
                },
                "client5": {
                    "description": "Desarrollo de la plataforma de E-commerce Starbucks Rewards para diferentes países como: Chile, Argentina y Colombia.",
                },
                "client6": {
                    "description": "gonet trabaja todos los días para mantener la seguridad dentro de la compañía con nuestros equipos de ciberseguridad y desarrollo de aplicaciones.",
                },
                "btnView": "Ver cliente",
            },
            "section2": {
                "title": "Casos de éxito",
                "legend1": "Estamos orgullosos de cada logro que hemos tenido junto a nuestros clientes.",
            },

        },
    }

}
export default {
    "en": {
        "start": {
            "title": "",
            "btn": "Start a Project",
        },
        "contact": {
            "title": "We love to hear your ideas and comments!",
            "btn": "Contact us",
        },
    },
    "es": {
        "start": {
            "title": "¿Listo para comenzar tu viaje con nosotros? ",
            "btn": "Comienza un proyecto",
        },
        "contact": {
            "title": "¡Nos encanta escuchar tus comentarios e ideas!",
            "btn": "Contáctanos",
        },
    },

}
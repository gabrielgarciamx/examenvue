export default {
    "en": {
        "meta": {
            "lang": "en-US",
            "locate": "en_US",
            "title": "About us - Gabriel",
            "description": "This is my gonet for Vue JS",
        },
        "info": {
            background: require('@/assets/images/about/principal-min.jpg'),
            //"background": "@/assets/images/about/principal.jpg",
            "title": "About me Gabriel",
            "legend": "This is my gonet for Vue JS",
        },

        "bg": {
            background1: require('@/assets/images/about/bg-experience.jpg'),
            background2: require('@/assets/images/about/bg-strategy.jpg'),
            background3: require('@/assets/images/about/bg-growth.jpg'),
        },
    },
    "es": {
        "meta": {
            "lang": "es-MX",
            "locate": "es_MX",
            "title": "Acerca de mi - Gabo",
            "description": "Este es mi gonet en Vue JS",
        },
        "info": {
            background: require('@/assets/images/about/principal-min.jpg'),
            //"background": "@/assets/images/about/principal.jpg",
            "title": "Quiénes somos",
            "legend": "gonet comenzó en 2008 con el objetivo de ayudar a diferentes industrias que buscan renovarse a través de la tecnología. Hemos trabajado para brindar las mejores soluciones y entendemos que para lograrlo tenemos que trabajar junto a las personas más talentosas, trabajamos constantemente para confirmar que: People are the way.",
        },

        "bg": {
            background1: require('@/assets/images/about/bg-experience.jpg'),
            background2: require('@/assets/images/about/bg-strategy.jpg'),
            background3: require('@/assets/images/about/bg-growth.jpg'),
        },
    }
}
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import 'sweetalert2/dist/sweetalert2.min.css'
import 'vue-sidebar-menu/dist/vue-sidebar-menu.css'
import 'vuetify/dist/vuetify.min.css'
import Vue from 'vue'
import Vuetify from 'vuetify'
import VueSweetalert2 from 'vue-sweetalert2'
import VueRouter from 'vue-router'
import Routes from './routes/routes'
import App from './App.vue'
import VueMeta from 'vue-meta';
import vSelect from 'vue-select';
import ToggleButton from 'vue-js-toggle-button'
import RangeSlider from 'vue-range-slider'
import VueSidebarMenu from 'vue-sidebar-menu'
import store from './library/store/store.js'
import VCalendar from 'v-calendar';
import vueVimeoPlayer from 'vue-vimeo-player';

import VeeValidate from 'vee-validate';
import Dictionaries from './validations/translates/languages';
import CV from './validations/Validations';

import './assets/icons/styles.css'
import { BFormGroup, BModal, BNavbar, BSidebar, NavbarPlugin, SidebarPlugin, BreadcrumbPlugin, BTab, BTabs, DropdownPlugin, TimePlugin, BButton, CalendarPlugin, BCard } from 'bootstrap-vue'
import i18n from '@/locales/i18n'
import VueI18n from 'vue-i18n'
import FlagIcon from 'vue-flag-icon'
import ElementUI from 'element-ui';
import ColorPicker from 'vue-color';
import MainLibrary from './library/MainLibrary.js';

import axios from 'axios';
import QrcodeVue from 'qrcode.vue';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { fab } from '@fortawesome/free-brands-svg-icons';
import { far } from '@fortawesome/free-regular-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import LoadScript from 'vue-plugin-load-script';
import VueScrollmagic from './plugins'


library.add(fas);
library.add(fab);
library.add(far);

Vue.use(Vuetify)
Vue.use(LoadScript);
Vue.use(FlagIcon);
Vue.use(VueI18n);
Vue.use(BreadcrumbPlugin);
Vue.use(DropdownPlugin);
Vue.use(CalendarPlugin);
Vue.use(TimePlugin);
Vue.config.productionTip = false
Vue.use(VueSweetalert2)
Vue.use(VueRouter)
Vue.use(ToggleButton)
Vue.use(VueSidebarMenu)
Vue.use(VCalendar, {componentPrefix: "vc"})
Vue.use(VueMeta);
Vue.use(NavbarPlugin);
Vue.use(SidebarPlugin);
Vue.use(vueVimeoPlayer);
Vue.use(VueScrollmagic)
Vue.use(VeeValidate);


VeeValidate.Validator.extend(CV.isBiggger.name, CV.isBiggger.validation , {hasTarget: CV.isBiggger.hasTarget });
VeeValidate.Validator.extend(CV.isBiggerOrEqual.name, CV.isBiggerOrEqual.validation , {hasTarget: CV.isBiggerOrEqual.hasTarget });
VeeValidate.Validator.extend(CV.isSmaller.name, CV.isSmaller.validation, {hasTarget: CV.isSmaller.hasTarget });
VeeValidate.Validator.extend(CV.isSmallerOrEqual.name, CV.isSmallerOrEqual.validation, {hasTarget: CV.isSmallerOrEqual.hasTarget });
VeeValidate.Validator.localize({...Dictionaries});
VeeValidate.Validator.localize(localStorage.getItem('languaje') || 'es');

Vue.config.productionTip = false

Vue.use(ElementUI, {
  i18n: (key, value) => i18n.t(key, value)
});

Vue.component('vSelect',vSelect);
Vue.component('RangeSlider',RangeSlider);
Vue.component('compactPicker', ColorPicker.Compact);
Vue.component('font-awesome-icon', FontAwesomeIcon);
Vue.component('b-form-group', BFormGroup)
Vue.component('b-modal', BModal);
Vue.component('b-navbar', BNavbar);
Vue.component('b-button', BButton);
Vue.component('qrcode-vue', QrcodeVue);
Vue.component('b-sidebar', BSidebar);
Vue.component('b-tabs', BTabs);
Vue.component('b-tab', BTab);
Vue.component('b-card', BCard);
const router = new VueRouter({
	mode: 'history',
	hash: false,
	routes: Routes
});
MainLibrary.addRequestInterceptors();
new Vue({
  el: '#app',
  i18n,
  store,
  render: h => h(App),
  router:router,
 
});

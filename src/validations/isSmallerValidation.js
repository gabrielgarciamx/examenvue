export default {
    name: 'isSmaller',
    validation: {
        validate: (value, [compared]) => value < compared
    },
    hasTarget: true
}
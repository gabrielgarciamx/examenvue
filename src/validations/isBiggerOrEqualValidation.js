export default {
    name: 'isBiggerOrEqual',
    validation: {
        validate: (value, [compared]) => value >= compared
    },
    hasTarget: true
}
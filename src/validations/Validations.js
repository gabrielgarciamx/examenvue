import { Validator } from 'vee-validate';
import Languages from "../validations/translates/languages";

import isBiggger from "./isBiggerValidation";
import isSmaller from "./isSmallerValidation";
import isBiggerOrEqual from "./isBiggerOrEqualValidation";
import isSmallerOrEqual from "./isSmallerOrEqualValidation";

let modules = {
    isBiggger,
    isSmaller,
    isBiggerOrEqual,
    isSmallerOrEqual
}

for (const key in modules) {
    if (modules.hasOwnProperty(key)) {
        const element = modules[key];
        element.validation.getMessage = Languages[Validator.locale].messages[element.name]
                                        ? (field, data) => Languages[Validator.locale].messages[element.name](field, data)
                                        : (field, data) => Languages[Validator.locale].messages['_default'](field)
    }
}

export default modules
export default {
    name: 'isBigger',
    validation: {
        validate: (value, [compared]) => value > compared
    },
    hasTarget: true
}
export default {
    name: 'isSmallerOrEqual',
    validation: {
        validate: (value, [compared]) => value <= compared
    },
    hasTarget: true
}
export const defaultIndustries = [
	{
		id: "Aeronautics",
		value: "Aeronautics",
	},
	{
		id: "Bank",
		value: "Bank",
	},
	{
		id: "Fintech",
		value: "Fintech",
	},
	{
		id: "Retail",
		value: "Retail",
	},
	{
		id: "Others",
		value: "Others",
	},
];
export const defaultEmployees = [
	{
		id: "0 - 10",
		value: "i18n0",
	},
	{
		id: "11 - 50",
		value: "i18n11",
	},
	{
		id: "51 - 100",
		value: "i18n51",
	},
	{
		id: "more 101",
		value: "i18n101",
	},
];
export const defaultCountries = [
	{
		id: "afghanistan",
		value: "afghanistan",
	},
	{
		id: "albania",
		value: "albania",
	},
	{
		id: "algeria",
		value: "algeria",
	},
	{
		id: "andorra",
		value: "andorra",
	},
	{
		id: "angola",
		value: "angola",
	},
	{
		id: "antiguaAndBarbuda",
		value: "antiguaAndBarbuda",
	},
	{
		id: "argentina",
		value: "argentina",
	},
	{
		id: "armenia",
		value: "armenia",
	},
	{
		id: "australia",
		value: "australia",
	},
	{
		id: "austria",
		value: "austria",
	},
	{
		id: "azerbaijan",
		value: "azerbaijan",
	},
	{
		id: "bahamas",
		value: "bahamas",
	},
	{
		id: "bahrain",
		value: "bahrain",
	},
	{
		id: "bangladesh",
		value: "bangladesh",
	},
	{
		id: "barbados",
		value: "barbados",
	},
	{
		id: "belarus",
		value: "belarus",
	},
	{
		id: "belgium",
		value: "belgium",
	},
	{
		id: "belize",
		value: "belize",
	},
	{
		id: "benin",
		value: "benin",
	},
	{
		id: "bhutan",
		value: "bhutan",
	},
	{
		id: "bolivia",
		value: "bolivia",
	},
	{
		id: "bosniaAndHerzegovina",
		value: "bosniaAndHerzegovina",
	},
	{
		id: "botswana",
		value: "botswana",
	},
	{
		id: "brazil",
		value: "brazil",
	},
	{
		id: "brunei",
		value: "brunei",
	},
	{
		id: "bulgaria",
		value: "bulgaria",
	},
	{
		id: "burkinaFaso",
		value: "burkinaFaso",
	},
	{
		id: "burundi",
		value: "burundi",
	},
	{
		id: "côteDIvoire",
		value: "côteDIvoire",
	},
	{
		id: "caboVerde",
		value: "caboVerde",
	},
	{
		id: "cambodia",
		value: "cambodia",
	},
	{
		id: "cameroon",
		value: "cameroon",
	},
	{
		id: "canada",
		value: "canada",
	},
	{
		id: "centralAfricanRepublic",
		value: "centralAfricanRepublic",
	},
	{
		id: "chad",
		value: "chad",
	},
	{
		id: "chile",
		value: "chile",
	},
	{
		id: "china",
		value: "china",
	},
	{
		id: "colombia",
		value: "colombia",
	},
	{
		id: "comoros",
		value: "comoros",
	},
	{
		id: "congoCongoBrazzaville",
		value: "congoCongoBrazzaville",
	},
	{
		id: "costaRica",
		value: "costaRica",
	},
	{
		id: "croatia",
		value: "croatia",
	},
	{
		id: "cuba",
		value: "cuba",
	},
	{
		id: "cyprus",
		value: "cyprus",
	},
	{
		id: "czechiaCzechRepublic",
		value: "czechiaCzechRepublic",
	},
	{
		id: "democraticRepublicOfTheCongo",
		value: "democraticRepublicOfTheCongo",
	},
	{
		id: "denmark",
		value: "denmark",
	},
	{
		id: "djibouti",
		value: "djibouti",
	},
	{
		id: "dominica",
		value: "dominica",
	},
	{
		id: "dominicanRepublic",
		value: "dominicanRepublic",
	},
	{
		id: "ecuador",
		value: "ecuador",
	},
	{
		id: "egypt",
		value: "egypt",
	},
	{
		id: "elSalvador",
		value: "elSalvador",
	},
	{
		id: "equatorialGuinea",
		value: "equatorialGuinea",
	},
	{
		id: "eritrea",
		value: "eritrea",
	},
	{
		id: "estonia",
		value: "estonia",
	},
	{
		id: "eswatiniFmrSwaziland",
		value: "eswatiniFmrSwaziland",
	},
	{
		id: "ethiopia",
		value: "ethiopia",
	},
	{
		id: "fiji",
		value: "fiji",
	},
	{
		id: "finland",
		value: "finland",
	},
	{
		id: "france",
		value: "france",
	},
	{
		id: "gabon",
		value: "gabon",
	},
	{
		id: "gambia",
		value: "gambia",
	},
	{
		id: "georgia",
		value: "georgia",
	},
	{
		id: "germany",
		value: "germany",
	},
	{
		id: "ghana",
		value: "ghana",
	},
	{
		id: "greece",
		value: "greece",
	},
	{
		id: "grenada",
		value: "grenada",
	},
	{
		id: "guatemala",
		value: "guatemala",
	},
	{
		id: "guinea",
		value: "guinea",
	},
	{
		id: "guineaBissau",
		value: "guineaBissau",
	},
	{
		id: "guyana",
		value: "guyana",
	},
	{
		id: "haiti",
		value: "haiti",
	},
	{
		id: "holySee",
		value: "holySee",
	},
	{
		id: "honduras",
		value: "honduras",
	},
	{
		id: "hungary",
		value: "hungary",
	},
	{
		id: "iceland",
		value: "iceland",
	},
	{
		id: "india",
		value: "india",
	},
	{
		id: "indonesia",
		value: "indonesia",
	},
	{
		id: "iran",
		value: "iran",
	},
	{
		id: "iraq",
		value: "iraq",
	},
	{
		id: "ireland",
		value: "ireland",
	},
	{
		id: "israel",
		value: "israel",
	},
	{
		id: "italy",
		value: "italy",
	},
	{
		id: "jamaica",
		value: "jamaica",
	},
	{
		id: "japan",
		value: "japan",
	},
	{
		id: "jordan",
		value: "jordan",
	},
	{
		id: "kazakhstan",
		value: "kazakhstan",
	},
	{
		id: "kenya",
		value: "kenya",
	},
	{
		id: "kiribati",
		value: "kiribati",
	},
	{
		id: "kuwait",
		value: "kuwait",
	},
	{
		id: "kyrgyzstan",
		value: "kyrgyzstan",
	},
	{
		id: "laos",
		value: "laos",
	},
	{
		id: "latvia",
		value: "latvia",
	},
	{
		id: "lebanon",
		value: "lebanon",
	},
	{
		id: "lesotho",
		value: "lesotho",
	},
	{
		id: "liberia",
		value: "liberia",
	},
	{
		id: "libya",
		value: "libya",
	},
	{
		id: "liechtenstein",
		value: "liechtenstein",
	},
	{
		id: "lithuania",
		value: "lithuania",
	},
	{
		id: "luxembourg",
		value: "luxembourg",
	},
	{
		id: "madagascar",
		value: "madagascar",
	},
	{
		id: "malawi",
		value: "malawi",
	},
	{
		id: "malaysia",
		value: "malaysia",
	},
	{
		id: "maldives",
		value: "maldives",
	},
	{
		id: "mali",
		value: "mali",
	},
	{
		id: "malta",
		value: "malta",
	},
	{
		id: "marshallIslands",
		value: "marshallIslands",
	},
	{
		id: "mauritania",
		value: "mauritania",
	},
	{
		id: "mauritius",
		value: "mauritius",
	},
	{
		id: "mexico",
		value: "mexico",
	},
	{
		id: "micronesia",
		value: "micronesia",
	},
	{
		id: "moldova",
		value: "moldova",
	},
	{
		id: "monaco",
		value: "monaco",
	},
	{
		id: "mongolia",
		value: "mongolia",
	},
	{
		id: "montenegro",
		value: "montenegro",
	},
	{
		id: "morocco",
		value: "morocco",
	},
	{
		id: "mozambique",
		value: "mozambique",
	},
	{
		id: "myanmarFormerlyBurma",
		value: "myanmarFormerlyBurma",
	},
	{
		id: "namibia",
		value: "namibia",
	},
	{
		id: "nauru",
		value: "nauru",
	},
	{
		id: "nepal",
		value: "nepal",
	},
	{
		id: "netherlands",
		value: "netherlands",
	},
	{
		id: "newZealand",
		value: "newZealand",
	},
	{
		id: "nicaragua",
		value: "nicaragua",
	},
	{
		id: "niger",
		value: "niger",
	},
	{
		id: "nigeria",
		value: "nigeria",
	},
	{
		id: "northKorea",
		value: "northKorea",
	},
	{
		id: "northMacedonia",
		value: "northMacedonia",
	},
	{
		id: "norway",
		value: "norway",
	},
	{
		id: "oman",
		value: "oman",
	},
	{
		id: "pakistan",
		value: "pakistan",
	},
	{
		id: "palau",
		value: "palau",
	},
	{
		id: "palestineState",
		value: "palestineState",
	},
	{
		id: "panama",
		value: "panama",
	},
	{
		id: "papuaNewGuinea",
		value: "papuaNewGuinea",
	},
	{
		id: "paraguay",
		value: "paraguay",
	},
	{
		id: "peru",
		value: "peru",
	},
	{
		id: "philippines",
		value: "philippines",
	},
	{
		id: "poland",
		value: "poland",
	},
	{
		id: "portugal",
		value: "portugal",
	},
	{
		id: "qatar",
		value: "qatar",
	},
	{
		id: "romania",
		value: "romania",
	},
	{
		id: "russia",
		value: "russia",
	},
	{
		id: "rwanda",
		value: "rwanda",
	},
	{
		id: "saintKittsAndNevis",
		value: "saintKittsAndNevis",
	},
	{
		id: "saintLucia",
		value: "saintLucia",
	},
	{
		id: "saintVincentAndTheGrenadines",
		value: "saintVincentAndTheGrenadines",
	},
	{
		id: "samoa",
		value: "samoa",
	},
	{
		id: "sanMarino",
		value: "sanMarino",
	},
	{
		id: "saoTomeAndPrincipe",
		value: "saoTomeAndPrincipe",
	},
	{
		id: "saudiArabia",
		value: "saudiArabia",
	},
	{
		id: "senegal",
		value: "senegal",
	},
	{
		id: "serbia",
		value: "serbia",
	},
	{
		id: "seychelles",
		value: "seychelles",
	},
	{
		id: "sierraLeone",
		value: "sierraLeone",
	},
	{
		id: "singapore",
		value: "singapore",
	},
	{
		id: "slovakia",
		value: "slovakia",
	},
	{
		id: "slovenia",
		value: "slovenia",
	},
	{
		id: "solomonIslands",
		value: "solomonIslands",
	},
	{
		id: "somalia",
		value: "somalia",
	},
	{
		id: "southAfrica",
		value: "southAfrica",
	},
	{
		id: "southKorea",
		value: "southKorea",
	},
	{
		id: "southSudan",
		value: "southSudan",
	},
	{
		id: "spain",
		value: "spain",
	},
	{
		id: "sriLanka",
		value: "sriLanka",
	},
	{
		id: "sudan",
		value: "sudan",
	},
	{
		id: "suriname",
		value: "suriname",
	},
	{
		id: "sweden",
		value: "sweden",
	},
	{
		id: "switzerland",
		value: "switzerland",
	},
	{
		id: "syria",
		value: "syria",
	},
	{
		id: "tajikistan",
		value: "tajikistan",
	},
	{
		id: "tanzania",
		value: "tanzania",
	},
	{
		id: "thailand",
		value: "thailand",
	},
	{
		id: "timorLeste",
		value: "timorLeste",
	},
	{
		id: "togo",
		value: "togo",
	},
	{
		id: "tonga",
		value: "tonga",
	},
	{
		id: "trinidadAndTobago",
		value: "trinidadAndTobago",
	},
	{
		id: "tunisia",
		value: "tunisia",
	},
	{
		id: "turkey",
		value: "turkey",
	},
	{
		id: "turkmenistan",
		value: "turkmenistan",
	},
	{
		id: "tuvalu",
		value: "tuvalu",
	},
	{
		id: "uganda",
		value: "uganda",
	},
	{
		id: "ukraine",
		value: "ukraine",
	},
	{
		id: "unitedArabEmirates",
		value: "unitedArabEmirates",
	},
	{
		id: "unitedKingdom",
		value: "unitedKingdom",
	},
	{
		id: "unitedStatesOfAmerica",
		value: "unitedStatesOfAmerica",
	},
	{
		id: "uruguay",
		value: "uruguay",
	},
	{
		id: "uzbekistan",
		value: "uzbekistan",
	},
	{
		id: "vanuatu",
		value: "vanuatu",
	},
	{
		id: "venezuela",
		value: "venezuela",
	},
	{
		id: "vietnam",
		value: "vietnam",
	},
	{
		id: "yemen",
		value: "yemen",
	},
	{
		id: "zambia",
		value: "zambia",
	},
	{
		id: "zimbabwe",
		value: "zimbabwe",
	},
];
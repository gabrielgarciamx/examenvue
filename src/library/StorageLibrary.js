export default{
    brandLocation: "",
    localStorage: window.localStorage,
    currentInfo: {},
    saleItemTemplate: {
        "unitPrice": 0.00,
        "itemType": 0,
        "cost": 0.00,
        "basePrice": 0.0,
        "name": "",
        "totalDiscount": 0.0,
        "quantity": 1,
        "productId": 3071,
        "discount": 0.0,
        "compositeProductList": [
            {
                "totalDiscount": 0.0,
                "quantity": 1,
                "sku": "5949641654681819",
                "unitPrice": 0.0,
                "productId": 0
            }
        ],
        "instructionList": [
            {
                "id": 0,
            }
        ],
        "totalAmount": "2300.50"
    },
    setBrandLocation(brandLocation)
    {
        this.brandLocation = brandLocation;
    },
    getBrandLocation()
    {
        return this.brandLocation;
    },
    saveInfo(value)
    {
        this.currentInfo = value;
        localStorage.setItem(this.getBrandLocation(),JSON.stringify(value));
    },
    getInfo() 
    {
        let tempInfo = localStorage.getItem(this.getBrandLocation()) ? JSON.parse(localStorage.getItem(this.getBrandLocation())) : {location: null, products: [], config: {}, order : {}, branch: {}, bussines: {id: 1427}, ecommerce: {}, token: false};
        this.currentInfo = tempInfo;
        return tempInfo;
    },
    setToken(token)
    {
        let info = this.getInfo();
        info.token = token;

        this.saveInfo(info);
    },
    getToken()
    {
        let info = this.getInfo();

        return info.token? info.token : false;
    },
    setEcommerce(ecommerce)
    {
        let info = this.getInfo();
        info.ecommerce = ecommerce;

        this.saveInfo(info);
    },
    getEcommerce()
    {
        let info = this.getInfo();

        return info.ecommerce? info.ecommerce : {};
    },
    setLocation(location)
    {
        let info = this.getInfo();
        info.location = location;

        this.saveInfo(info);
    },
    getLocation()
    {
        let info = this.getInfo();

        return info.location? info.location : "";
    },
    setBussines(bussines){
        let info = this.getInfo();
        info.bussines = bussines;

        this.saveInfo(info);
    },
    getBussines(){
        let info = this.getInfo();

        return info.bussines || {};
    },
    setBranch(branch)
    {
        let info = this.getInfo();
        info.branch = branch;

        this.saveInfo(info);
    },
    getBranch()
    {
        let info = this.getInfo();

        return info.branch? info.branch : {};
    },
    getProducts()
    {
        let info = this.getInfo();

        return info.products? info.products : [];
    },
    getConfig()
    {
        let info = this.getInfo();

        return info.config? info.config : {};
    },
    getOrder()
    {
        let info = this.getInfo();

        return info.order ? info.order : {};
    },
    addNewProduct(product)
    {
        let info = this.getInfo();

        if(!info.products)
        {
            info.products = [];
        }

        info.products.push(product);
        this.saveInfo(info);
        return info.products;
    },
    addQuantityProduct(position,toAdd)
    {
        let info = this.getInfo();

        if(!info.products)
        {
            info.products = [];
        }

        info.products[position].quantity += toAdd;
        this.saveInfo(info);
        return info.products;
    },
    getSaleObservations() {
        let info = this.getInfo();

        return info.saleObservation ? info.saleObservation : "";
    },
    setSaleObservations(observation) {
        let info = this.getInfo();

        if(!info.saleObservation)
        {
            info.saleObservation = "";
        }

        info.saleObservation = observation;
        this.saveInfo(info);
        return info.saleObservation ? info.saleObservation : "";
    },
    setAllProducts(products)
    {
        let info = this.getInfo();

        if(!info.products || !info.products.length)
        {
            return;
        }
        info.products = products;
        this.saveInfo(info);
        return info.products;
    },
    setConfig(config) {
        let info = this.getInfo();

        if(!info.config) {
            return;
        }

        info.config = config;
        this.saveInfo(info);

        return info.config;
    },
    setOrder(order) {
        let info = this.getInfo();

        if(!info.order) {
            return;
        }

        info.order = order;
        this.saveInfo(info);

        return info.order;
    },
    cleanOrder() {
        let info = this.getInfo();

        info.order = {};
        this.saveInfo(info);

        return info.order;
    },
    cleanProducts()
    {
        let info = this.getInfo();

        info.products = [];
        this.saveInfo(info);
        return info.products;
    },

}
import {Keccak} from 'sha3'; // https://www.npmjs.com/package/sha3

export default{
    generateUID(email)
    {
        return "web"+(email.replace(/[^0-9A-Z]+/gi,""));
    },
    getHashPassword(password)
    {
        let hash = new Keccak(512);
        hash.update(password);
        var passwordDigest =hash.digest('hex');
        // => hash of 'hello'
        
        hash.reset();
        return passwordDigest;
    }
}
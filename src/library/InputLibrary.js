export default {
    regexNumber: /[0-9]/i,
    regexDecimalNumber: /^[0-9]+(\.)?([0-9][0-9]?)?$/i,
    regexText: /[a-zA-Z áéíóúÁÉÍÓÚñÑ]/i,
    regexTextNumber: /[a-zA-Z0-9 áéíóúÁÉÍÓÚñÑ]/i,
    regexNoSpaces: /^\S+$/i,
    regexNoSpaceOrSpecialChars: /^(\d|\w)+$/i,
    email: /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/i,
    keyIsNumber(event,acceptPoint){
        if(event.key=="." && acceptPoint)
        {
            return true;
        }
        if(!RegExp(this.regexNumber).test(event.key) )
        {
            return false;
        }
        return true;
    },
    StringIsDecimalNumber(str) {
        if(!RegExp(this.regexDecimalNumber).test(str))
        {
            return false;
        }
        return true;
    },
    keyIsText(event){
        if(!RegExp(this.regexText).test(event.key))
        {
            return false;
        }
        return true;
    },
    keyIsTextOrNumber(event){
        if(!RegExp(this.regexTextNumber).test(event.key))
        {
            return false;
        }
        return true;
    },
    keyIsNoSpaces(event){
        if(!RegExp(this.regexNoSpaces).test(event.key))
        {
            return false;
        }
        return true;
    },
    keyIsNoSpaceOrSpecialChars(event){
        if(!RegExp(this.regexNoSpaceOrSpecialChars).test(event.key))
        {
            return false;
        }
        return true;
    }
}
import store from '../../library/store/store.js';
import Validator from 'vee-validate';
import language from '../../library/langChioceLibrary';
import InputLibrary from '../../library/InputLibrary.js';
import Routes from '../../routes/routes';
import i18n from '../../locales/i18n';

export default {
	methods: {
        beforeMountLang: function () {
            let fullPath = window.location.href;
            let locale = language['default'];
            Object.getOwnPropertyNames(language).forEach((lang) => {
                if (fullPath.indexOf(lang) !== -1) {
                    locale = language[lang];
                }
            });
            i18n.locale = locale;
            Validator.Validator.localize(locale);
            store.dispatch('translate/setLanguaje',locale);
        },
        handleDrop(event) {
            let dt = event.dataTransfer;
            let files = dt.files;
            let fileFieldName = this.fileFieldName !== undefined ? this.fileFieldName : "fileField";
            this.handleFiles(files, fileFieldName);
        },
        onSelectFile(event, fileInputName, fileFieldName) {
            const input = this.$refs[fileInputName];
            const files = input.files;
            this.handleFiles(event, files, fileFieldName);
        },
        handleFiles(event, files, fileFieldName) {
            let valid = false;
            let validExtentions = [];
            let maxFileSize = 0;

            if (this.maxFileSize == undefined || this.maxFileSize == null) {
                maxFileSize = (1024 * 1024) * 3;
            }

            if (this.validExtentions == undefined || this.validExtentions == null) {
                validExtentions = ['application/pdf'];
            } else if (typeof this.validExtentions === 'string'){
                validExtentions = [this.validExtentions];
            } else if (Array.isArray(this.validExtentions)) {
                validExtentions = this.validExtentions;
            }
            if (files && files[0]) {
                if (validExtentions.includes(files[0]['type'])) {
                    if (files[0].size <= maxFileSize) {
                        const reader = new FileReader;
                        reader.onload = e => {
                            // this.imageData = e.target.result;
                            this.compModel[fileFieldName] = {
                                type: 'file',
                                file: files[0],
                                base: e.target.result,
                                data: e.target.result.replace(/^data:.+;base64,/, '')
                            };
                            // this.imageDataToSend = e.target.result.replace(/^data:.+;base64,/, '');
                        }
                        reader.readAsDataURL(files[0]);
                        valid = true;
                        if (this.doIsValidFile) {
                            this.doIsValidFile();
                        }
                    } else {
                        console.log('Invalid file size');
                    }
                } else {
                    console.log('Invalid file type');
                }
            } else {
                console.log('Empty file');
            }

            if (!valid) {
                event.preventDefault();
                event.stopPropagation();
                if (this.alertInvalidFile) {
                    this.alertInvalidFile();
                }
            }
        },
        commonStartProject(event) {
            if (event) {
                event.preventDefault();
                event.stopPropagation();
            }
            this.$root.$emit('on-start-project');
        },
        onlyNumber(event) {
            if(!InputLibrary.keyIsNumber(event)) {
                event.preventDefault();
                event.stopPropagation();
                return;
            }
        },
        getRouteInfo() {
            let founded = this.searchRoute(this.$route,Routes);
            return {
                route: this.$route,
                routeItem: founded
            };
        },
        searchRoute(route, items) {
            let that = this;
            let founded = undefined;
            items.every((item) => {
                if (item.name == route.name) {
                    founded = item;
                    return false;
                } else if (item.hasOwnProperty('children') && item.children.length > 0) {
                    founded = that.searchRoute(route, item.children);
                    return (founded === undefined);
                } else {
                    return true;
                }
            });
            return founded;
        },
		Upperfirst(string) {
            return string.charAt(0).toUpperCase() + string.slice(1);
        },
        GoToTop() {
            let container = document.querySelector(".content.dashboard-content");
            container.scrollTo(0, 0);
        },
        dynamicSort(property) {
            var sortOrder = 1;

            if(property[0] === "-") {
                sortOrder = -1;
                property = property.substr(1);
            }

            return function (a,b) {
                if ((typeof a[property]) == "string") {
                    if(sortOrder == -1){
                        return b[property].trim().localeCompare(a[property].trim());
                    }else{
                        return a[property].trim().localeCompare(b[property].trim());
                    }
                }else{
                    if(sortOrder == -1){
                        if (a[property] > b[property]) return -1;
                        if (a[property] < b[property]) return 1;
                        return 0;
                    }else{
                        if (a[property] < b[property]) return -1;
                        if (a[property] > b[property]) return 1;
                        return 0;
                    }
                }
            }
        },
        trimChar(originalString, toRevomeString) {
			if (toRevomeString === "]") toRevomeString = "\\]";
			if (toRevomeString === "\\") toRevomeString = "\\\\";
			return originalString.replace(new RegExp("^[" + toRevomeString + "]+|[" + toRevomeString + "]+$", "g"), "");
		},
		cloneJsonObj(jsonObj) {
			return JSON.parse(JSON.stringify(jsonObj));
		},
        setAsCashHtmlFormat(amount) {
            amount = this.setAsCashFormat(amount);
            let separate = amount.split(".");
            amount = separate[0];
            if (separate.length > 1) {
                amount += ".<sup>" + separate[1] + "</sup>";
            }

            return amount;
        },
        setAsCashFormat(amount) {
            if (typeof amount === "string") {
                return amount;
            }
            amount = parseFloat(amount).toString();

            let separate = amount.split(".");
            if (separate.length > 1) {
                let zeros = "";
                for (var i = separate[1].length; i < 2; i++) {
                    zeros += "0";
                }

                return "$" + separate[0] + "." + separate[1] + zeros;
            } else {
                return "$" + amount + ".00";
            }
        },
        toDateFormatDDMMYY(date) {
            let today = new Date(Date.parse(date));
            // return today.toLocaleDateString();

            let dd = String(today.getDate()).padStart(2, '0');
            let mm = String(today.getMonth() + 1).padStart(2, '0');
            let yyyy = today.getYear() - 100;
            // let yyyy = today.getFullYear();

            let finalDate = dd + '/' + mm + '/' + yyyy;
            return finalDate;
        },
        formatDate(dateget){
            if (typeof dateget === "string") {
                return dateget;
            }
            const d = new Date(dateget);
            const mo = new Intl.DateTimeFormat('en', { month: 'short' }).format(d).toUpperCase();

            return ("0" + d.getDate()).slice(-2) + "-" + mo + " " + ("0" + d.getHours()).slice(-2) + ":" + ("0" + d.getMinutes()).slice(-2);
        },
	}
}
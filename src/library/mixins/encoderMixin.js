import { Keccak } from 'sha3';

export default {
	methods: {
		encode(pass) {
			let hash = new Keccak(512);

			hash.update(pass);
			return hash.digest({ buffer: Buffer.alloc(64), format: 'hex' });
		},
	}
}
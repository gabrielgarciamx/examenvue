import CommonMixin from '../../library/mixins/commonMixin.js';

export default {
    mixins: [CommonMixin],
    methods: {
        Question(title, text, btnOk, btnCancel, okCallback, cancelCallback, reverseStyles) {
            if (btnOk === undefined || btnOk === null) {
                btnOk = this.$t("interface.accept");
            }
            if (btnCancel === undefined || btnCancel === null) {
                btnCancel = this.$t("interface.back");
            }

            title = title ? this.Upperfirst(title) : "";
            text = text ? this.Upperfirst(text) : "";

            let customClass = {
                actions: "d-flex justify-content-between w-100",
                confirmButton: "shp-btn-deleteButton",
                cancelButton: "shp-btn-secondary",
            };

            if (reverseStyles) {
                customClass.cancelButton = "shp-btn-deleteButton ";
                customClass.confirmButton = "shp-btn-secondary ";
            }
            this.$swal({
                type: "warning",
                title: title,
                html: text,
                confirmButtonText: btnOk,
                showCancelButton: true,
                cancelButtonText: btnCancel,
                reverseButtons: true,
                customClass: customClass,
                allowOutsideClick: false,
                allowEscapeKey: false,
                onOpen: function (component) {
                    let alertCircle = component.getElementsByClassName("swal2-icon swal2-warning");
                    if (alertCircle.length > 0) {
                        alertCircle[0].innerHTML = '<h1 class="circle-icon">!</h1>'; 
                    }
                }
            })
            .then(result => {
                if(result.value && typeof okCallback === "function") {
                    okCallback();
                } else if (result.dismiss == 'cancel' && typeof cancelCallback === "function") {
                    cancelCallback();
                }
            });
        },
        OptionsQuestion(title, text, btns) {
            if (!(Array.isArray(btns) && btns.length > 0)) {
                return;
            }

            title = title ? this.Upperfirst(title) : "";
            text = text ? this.Upperfirst(text) : "";
            
            let that = this;
            let response = undefined;
            let afterFuncCallback = undefined;
            this.$swal({
                type: "warning",
                title: title,
                html: text,
                showCloseButton: true,
                showCancelButton: false,
                showConfirmButton: false,
                allowOutsideClick: false,
                allowEscapeKey: false,
                onOpen: async function (component) {
                    let alertCircle = component.getElementsByClassName("swal2-icon swal2-warning");
                    if (alertCircle.length > 0) {
                        alertCircle[0].innerHTML = '<h1 class="circle-icon">!</h1>'; 
                    }
                    let content = component.getElementsByClassName("swal2-content");
                    let rows = Math.ceil(btns.length / 2);
                    let rowBtns = [];
                    if (content.length > 0) {
                        for (var i = 0; i < rows; i++) {
                            content[0].insertAdjacentHTML( 'afterend', '<div class="swal2-actions row-custom-actions d-flex justify-content-between w-100"></div>' );
                        }
                    }
                    let actions = component.getElementsByClassName("swal2-actions");
                    if (content.length > 0) {
                        btns.forEach((btn, index) => {
                            let button = document.createElement('BUTTON');
                            button.className = "swal2-cancel ";
                            // button.className = "swal2-cancel swal2-styled";
                            if (btn.hasOwnProperty("class") && btn.class.trim() != "") {
                                button.className += " " + btn.class;
                            } else {
                                button.className += " shp-btn-secondary";
                            }
                            if (btn.hasOwnProperty("text")) {
                                button.innerHTML = btn.text;
                            }
                            button.onclick = async function () {
                                that.$swal({showConfirmButton: false}).close();
                                if (btn.hasOwnProperty("action") && typeof btn.action === "function") {
                                    if (btn.hasOwnProperty("isAsync") && btn.isAsync) {
                                        await btn.action()
                                        .then(axiosResponse => {
                                            response = axiosResponse;
                                            if(btn.hasOwnProperty("callback") && typeof btn.callback === "function") {
                                                btn.callback(response);
                                            }
                                        }).catch(error => {
                                            // this.errorMessage = error.message;
                                            // console.error("There was an error!", error);
                                            let message = undefined;
                                            if (error && error.response && error.response.status && error.response.status != 200) {
                                                message = error.message;
                                            }
                                            let failCallback = undefined;
                                            if(btn.hasOwnProperty("failCallback") && typeof btn.failCallback === "function") {
                                                failCallback = function () {
                                                    btn.failCallback(error);
                                                }
                                            }
                                            that.LoadFail(message, failCallback);
                                        });
                                    } else {
                                        response = await btn.action();
                                        if(btn.hasOwnProperty("callback") && typeof btn.callback === "function") {
                                            btn.callback(response);
                                        }
                                    }
                                }
                            }
                            let row = Math.floor(index / 2);
                            actions[row].appendChild(button);
                        });
                    }
                }
            });
        },
        QuestionRouteLeave(to, from , next) {
            /* eslint-disable no-console */
            console.log('to', to);
            if (typeof this.isDirty === "function" && this.isDirty()){
                this.$swal.fire({
                    title: this.$t('alert.confirmLeaveCreateForm.title'),
                    text: this.$t('alert.confirmLeaveCreateForm.message'),
                    type: 'warning',
                    showCancelButton: true,
                    reverseButtons: true,
                    customClass: {
                        confirmButton: "shp-btn-primary  ml-4",
                        cancelButton: "shp-btn-secondary  mr-4"
                    },
                    confirmButtonText: this.$t('alert.confirmLeaveCreateForm.confirmButton'),
                    cancelButtonText: this.$t('alert.confirmLeaveCreateForm.cancelButton'),
                    onOpen: function (component) {
                        let alertCircle = component.getElementsByClassName("swal2-icon swal2-warning");
                        if (alertCircle.length > 0) {
                            alertCircle[0].innerHTML = '<h1 class="circle-icon">!</h1>';
                        }
                    }
                })
                .then((result) => {
                    if (result.value) {
                        next()
                    } else {
                        next(false)
                    }
                });
            } else {
                next();
            }
        },
        LoadAlert(title, text, isAsync, funcCallback, afterFuncCallback, afterFailFuncCallback) {
            title = title ? this.Upperfirst(title) : "";
            text = text ? this.Upperfirst(text) : "";

            let that = this;
            let response = undefined;
            this.$swal({
                title: title,
                html: text,
                showConfirmButton: false,
                allowOutsideClick: false,
                allowEscapeKey: false,
                onOpen: async function (component) {
                    let alerts = component.getElementsByClassName("swal2-header");
                    let alertCircle = component.getElementsByClassName("swal2-icon swal2-loading");
                    let alertCircleWarning = component.getElementsByClassName("swal2-icon swal2-warning");
                    if (alertCircle.length <= 0 && alertCircleWarning.length > 0) {
                        if (alerts.length > 0) {
                            alerts[0].insertAdjacentHTML( 'afterbegin', '<div class="swal2-icon swal2-loading" style="display: block;"></div>' );
                        }
                    }
                    alertCircle = component.getElementsByClassName("swal2-icon swal2-loading");
                    if (alertCircle.length > 0) {
                        alertCircle[0].innerHTML = '<div class="loader"></div>'; 
                    }
                    // that = this;
                    if (typeof funcCallback === "function") {
                        if (isAsync) {
                            await funcCallback()
                            .then(axiosResponse => {
                                response = axiosResponse;
                                that.$swal({showConfirmButton: false}).close();
                                if(typeof afterFuncCallback === "function") {
                                    afterFuncCallback(response);
                                }
                            }).catch(error => {
                                // this.errorMessage = error.message;
                                console.error("There was an error!", error);
                                let message = undefined;
                                if (error && error.response && error.response.status && error.response.status != 200) {
                                    message = error.message;
                                }
                                let failCallback = undefined;
                                if(typeof afterFailFuncCallback === "function") {
                                    failCallback = function () {
                                        afterFailFuncCallback(error);
                                    }
                                }
                                that.$swal({showConfirmButton: false}).close();
                                that.LoadFail(message, failCallback)
                            });
                        } else {
                            setTimeout(() => {
                                that.$swal({showConfirmButton: false}).close();
                            }, 1000);
                            response = await funcCallback();
                            if(typeof afterFuncCallback === "function") {
                                afterFuncCallback(response);
                            }
                        }
                    } else if(typeof afterFailFuncCallback === "function") {
                        setTimeout(() => {
                            that.$swal({showConfirmButton: false}).close();
                        }, 1000);
                        afterFailFuncCallback(undefined);
                    } else {
                        that.$swal({showConfirmButton: false}).close();
                    }
                }
            });
        },
        SuccessAlert(title, text, btnOk, okCallback) {
            if (btnOk === undefined || btnOk === null) {
                btnOk = this.$t("interface.ok");
            }

            title = title ? this.Upperfirst(title) : "";
            text = text ? this.Upperfirst(text) : "";

            this.$swal({
                type: 'success',
                title: title,
                html: text,
                confirmButtonText: btnOk,
                customClass: {
                    confirmButton: "shp-btn-primary",
                },
                allowOutsideClick: false,
                allowEscapeKey: false,
            })
            .then(result=> {
                if(result.value && typeof okCallback === "function") {
                    okCallback();
                }
            });
        },
        FailAlert(title, text, btnOk, okCallback) {
            if (btnOk === undefined || btnOk === null) {
                btnOk = this.$t("interface.accept");
            }

            title = title ? this.Upperfirst(title) : "";
            text = text ? this.Upperfirst(text) : "";

            this.$swal({
                type: 'error',
                title: title,
                html: text,
                confirmButtonText: btnOk,
                customClass: {
                    confirmButton: "shp-btn-primary",
                },
                allowOutsideClick: false,
                allowEscapeKey: false,
            })
            .then(result=> {
                if(result.value && typeof okCallback === "function") {
                    okCallback();
                }
            });
        },
        ConfirmQuestion(title, text, btnOk, btnCancel, okCallback, cancelCallback) {
            this.Question(title,text, btnOk, btnCancel, okCallback, cancelCallback, true);
        },
        AlertQuestion(title, text, btnOk, btnCancel, okCallback, cancelCallback) {
            this.Question(title,text, btnOk, btnCancel, okCallback, cancelCallback, false);
        },
        DisableInModuleQuestion(entityName, name, dependencyEntityName, dependencyName, okCallback, cancelCallback) {7
            let title = this.$t('alert.disableInModule.common.title');
            let text = this.$t('alert.disableInModule.common.text');
            if (entityName && name && dependencyEntityName && dependencyName) {
                title = this.$t('alert.disableInModule.custom.title').split("{entityName}").join(entityName).split("{name}").join(name).split("{dependencyEntityName}").join(dependencyEntityName).split("{dependencyName}").join(dependencyName);
                text = this.$t('alert.disableInModule.custom.text').split("{entityName}").join(entityName).split("{name}").join(name).split("{dependencyEntityName}").join(dependencyEntityName).split("{dependencyName}").join(dependencyName);
            }
            let btnOk = this.$t("interface.disable");
            this.AlertQuestion(title,text, btnOk, undefined,okCallback, cancelCallback);
        },
        DisableQuestion(entityName, okCallback, cancelCallback) {
            let title = entityName ? this.$t('alert.disable.custom.title').split("{entityName}").join(entityName) : this.$t('alert.disable.common.title');
            let text = entityName ? this.$t('alert.disable.custom.text').split("{entityName}").join(entityName) : this.$t('alert.disable.common.text');
            let btnOk = this.$t("interface.disable");
            this.AlertQuestion(title,text, btnOk, undefined,okCallback, cancelCallback);
        },
        DeleteQuestion(entityName, okCallback, cancelCallback) {
            let title = entityName ? this.$t('alert.delete.custom.title').split("{entityName}").join(entityName) : this.$t('alert.delete.common.title');
            let text = entityName ? this.$t('alert.delete.custom.text').split("{entityName}").join(entityName) : this.$t('alert.delete.common.text');
            let btnOk = this.$t("interface.remove");
            this.AlertQuestion(title,text, btnOk, undefined,okCallback, cancelCallback);
        },
        SaveSuccess(entityName, okCallback) {
            let title = entityName ? this.$t('alert.save.custom.success.title').split("{entityName}").join(entityName) : this.$t('alert.save.common.success.title');
            let text = entityName ? this.$t('alert.save.custom.success.text').split("{entityName}").join(entityName) : this.$t('alert.save.common.success.text');
            this.SuccessAlert(title,text, undefined, okCallback);
        },
        BulkSuccess(entityName, okCallback) {
            let title = entityName ? this.$t('alert.bulk.custom.success.title').split("{entityName}").join(entityName) : this.$t('alert.bulk.common.success.title');
            let text = entityName ? this.$t('alert.bulk.custom.success.text').split("{entityName}").join(entityName) : this.$t('alert.bulk.common.success.text');
            this.SuccessAlert(title,text, undefined, okCallback);
        },
        EnableSuccess(entityName, okCallback) {
            let title = entityName ? this.$t('alert.enable.custom.success.title').split("{entityName}").join(entityName) : this.$t('alert.enable.common.success.title');
            let text = entityName ? this.$t('alert.enable.custom.success.text').split("{entityName}").join(entityName) : this.$t('alert.enable.common.success.text');
            this.SuccessAlert(title,text, undefined, okCallback);
        },
        DisableSuccess(entityName, okCallback) {
            let title = entityName ? this.$t('alert.disable.custom.success.title').split("{entityName}").join(entityName) : this.$t('alert.disable.common.success.title');
            let text = entityName ? this.$t('alert.disable.custom.success.text').split("{entityName}").join(entityName) : this.$t('alert.disable.common.success.text');
            this.SuccessAlert(title,text, undefined, okCallback);
        },
        DeleteSuccess(entityName, okCallback) {
            let title = entityName ? this.$t('alert.delete.custom.success.title').split("{entityName}").join(entityName) : this.$t('alert.delete.common.success.title');
            let text = entityName ? this.$t('alert.delete.custom.success.text').split("{entityName}").join(entityName) : this.$t('alert.delete.common.success.text');
            this.SuccessAlert(title,text, undefined, okCallback);
        },
        LoadFail(returnedText, okCallback) {
            let title = this.$t('alert.load.common.fail.title');
            let text = this.$t('alert.load.common.fail.text');
            text = returnedText ? returnedText : text;
            this.FailAlert(title,text, undefined, okCallback);
        },
        SaveFail(entityName, returnedText, okCallback) {
            let title = entityName ? this.$t('alert.save.custom.fail.title').split("{entityName}").join(entityName) : this.$t('alert.save.common.fail.title');
            let text = entityName ? this.$t('alert.save.custom.fail.text').split("{entityName}").join(entityName) : this.$t('alert.save.common.fail.text');
            text = returnedText ? returnedText : text;
            this.FailAlert(title,text, undefined, okCallback);
        },
        BulkFail(entityName, returnedText, okCallback) {
            let title = entityName ? this.$t('alert.bulk.custom.fail.title').split("{entityName}").join(entityName) : this.$t('alert.bulk.common.fail.title');
            let text = entityName ? this.$t('alert.bulk.custom.fail.text').split("{entityName}").join(entityName) : this.$t('alert.bulk.common.fail.text');
            text = returnedText ? returnedText : text;
            this.FailAlert(title,text, undefined, okCallback);
        },
        DisableFail(entityName, returnedText, okCallback) {
            let title = entityName ? this.$t('alert.disable.custom.fail.title').split("{entityName}").join(entityName) : this.$t('alert.disable.common.fail.title');
            let text = entityName ? this.$t('alert.disable.custom.fail.text').split("{entityName}").join(entityName) : this.$t('alert.disable.common.fail.text');
            text = returnedText ? returnedText : text;
            this.FailAlert(title,text, undefined, okCallback);
        },
        EnableFail(entityName, returnedText, okCallback) {
            let title = entityName ? this.$t('alert.enable.custom.fail.title').split("{entityName}").join(entityName) : this.$t('alert.enable.common.fail.title');
            let text = entityName ? this.$t('alert.enable.custom.fail.text').split("{entityName}").join(entityName) : this.$t('alert.enable.common.fail.text');
            text = returnedText ? returnedText : text;
            this.FailAlert(title,text, undefined, okCallback);
        },
        DeleteFail(entityName, returnedText, okCallback) {
            let title = entityName ? this.$t('alert.delete.custom.fail.title').split("{entityName}").join(entityName) : this.$t('alert.delete.common.fail.title');
            let text = entityName ? this.$t('alert.delete.custom.fail.text').split("{entityName}").join(entityName) : this.$t('alert.delete.common.fail.text');
            text = returnedText ? returnedText : text;
            this.FailAlert(title,text, undefined, okCallback);
        },
    }
}
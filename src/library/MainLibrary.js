import axios from 'axios';
import constants from './ConstantsLibrary.js';
import StorageLibrary from './StorageLibrary.js';

export default{
    addRequestInterceptors()
    {
        var _this = this;

        axios.interceptors.request.use((config) => {
            //config.url = config.url.replace("{businessId}",StorageLibrary.getBussines().id);
            if((config.data && !config.data.externalRequest) || (!config.data && !config.headers['external-request']))
            {
                config.url = constants.serviceUrl+config.url;
            }
            let session = localStorage.getItem('jsessionId');
            let businessId = localStorage.getItem('businessId');
            if(session)
            {
                if(!config.data)
                {
                    config.data = {};
                }
                /*config.data.jsessionId  = session;
                if(businessId)
                {
                    config.data.businessId = businessId;
                }/**/
            }
            return config;
        });
    },
}
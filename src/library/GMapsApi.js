import axios from 'axios';
import ConstantsLibrary from '../library/ConstantsLibrary.js';

export default{
    distanceMatrixUrl: "https://maps.googleapis.com/maps/api/distancematrix/json",
    apiKey: ConstantsLibrary.googleApiKey,
    // https://maps.googleapis.com/maps/api/distancematrix/json?origins=19.388771,-99.1724761&destinations=19.4290356,-99.1631967|19.4303051,-99.1602797&key=AIzaSyDWqAgyc3q9stcUuPlgJvbye_oLubkrk_w
    /**
     * 
     * @param Array[] origins => ['19.388771,-99.1724761','19.388771,-99.1724761']
     * @param Array[] destinations  => ['19.388771,-99.1724761','19.388771,-99.1724761']
     * @param Object{} options => {
     *  
     * }
     */
    distanceMatriz: function(origins, destinations)
    {
        let originsParam = "";
        let destinationsParam = "";
        let url = this.distanceMatrixUrl + "?";
        for(let i=0;i<origins.length;i++)
        {
            originsParam += origins[i];
            if(origins.length>1 && i<(origins.length-1))
            {
                originsParam += "|";
            }
        }

        for(let i=0;i<destinations.length;i++)
        {
            destinationsParam+= destinations[i];
            if(destinations.length>1 && i<(destinations.length-1))
            {
                destinationsParam += "|";
            }
        }

        url+= "origins="+originsParam;
        url+= "&destinations="+destinationsParam;
        url+= "&key="+this.apiKey;

        return axios.get(url, {
            headers: {
                'external-request': true,
            }
        });
    }
};
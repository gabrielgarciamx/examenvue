// import { createStore } from "vuex"
// import AuthStore from './AuthStore';
// import TranslateStore from './TranslateStore.js'
// import ProductsStore from './ProductsStore.js';

// const store = createStore({
//   state: {
    
//   },
//   modules: {
//     auth: AuthStore,
//     products: ProductsStore,
//     translate: TranslateStore,
//   }
// });

// export default store;

import Vue from 'vue'
import Vuex from 'vuex';
import TranslateStore from './TranslateStore.js'

Vue.use(Vuex)
export default new Vuex.Store({
  state: {
    
  },
  modules: {
    translate: TranslateStore,
  }
});
  
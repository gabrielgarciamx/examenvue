import Constants from '../ConstantsLibrary.js';
//import i18n from '@/plugins/i18n';

export default {
    namespaced: true,

    // module assets
    state: { 
        languaje: window.localStorage.getItem('languaje')?window.localStorage.getItem('languaje'):Constants.languajeDefault,
    },
    getters: {
        getLanguaje(state){ return state.languaje},
    },
    mutations: {
        setLanguaje(state,languaje){
            state.languaje = languaje;
            window.localStorage.setItem('languaje',languaje);
        }
    },
    actions: {
        async setLanguaje(context,languaje){
            return await context.commit('setLanguaje',languaje);
        }
    }
}
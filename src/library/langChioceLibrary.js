export default {
    'default': 'en',
    '/us/': 'en',
    '/mx/': 'es',
    '/es/': 'es',
}
import NotFound from '../pages/error.vue';
import Main from '../pages/Main.vue';
import axios from 'axios';
import i18n from '@/locales/i18n'


import Home from "./Children/Home";
/*Children*/
import signUpChildren from "./Children/signUpChildren";
import signInChildren from "./Children/signInChildren";
import losPasswordChildren from "./Children/losPasswordChildren";
import aboutUsChildren from "./Children/aboutUsChildren";
import profileChildren from "./Children/profileChildren";

const SUPPORTED_LOCALES = [{
    code: 'en',
    base: '',
    flag: 'us',
    name: 'English',
}, {
    code: 'es',
    base: '/es',
    flag: 'es',
    name: 'español',
}]

function getLocaleRegex() {
    let reg = '';
    SUPPORTED_LOCALES.forEach((locale, index) => {
        reg = `${reg}${locale.code}${index !== SUPPORTED_LOCALES.length - 1 ? '|' : ''}`;
    });
    return `(${reg})`;
}

/*PAGES */
let homePage = {
    path: `/:locale${getLocaleRegex()}?/`,
    //name:'home',
    component: Main,
    isHere: false,
    showMenu: true,
    classIcon: "icon iconpos-order",
    meta: {
        isPublic: true,
    },
    children: Home

}

let signUp = {
    path: `/:locale${getLocaleRegex()}?/sign-up`,
    // path: '/example',
    //name: 'our-services',
    component: Main,
    isHere: false,
    showMenu: true,
    classIcon: "icon iconpos-order",
    meta: {
        isPublic: true,
    },
    children: signUpChildren,
}

let signIn = {
    path: `/:locale${getLocaleRegex()}?/sign-in`,
    // path: '/example',
    //name: 'technologies',
    component: Main,
    isHere: false,
    showMenu: true,
    classIcon: "icon iconpos-order",
    meta: {
        isPublic: true,
    },
    children: signInChildren,
}

let lospassword = {
    path: `/:locale${getLocaleRegex()}?/lost-password`,
    // path: '/example',
    //name: 'industries',
    component: Main,
    isHere: false,
    showMenu: true,
    classIcon: "icon iconpos-order",
    meta: {
        isPublic: true,
    },
    children: losPasswordChildren,
}

let aboutUs = {
    path: `/:locale${getLocaleRegex()}?/about-us`,
    // path: '/example',
    //name: 'aboutUs',
    component: Main,
    isHere: false,
    showMenu: true,
    classIcon: "icon iconpos-order",
    meta: {
        isPublic: true,
    },
    children: aboutUsChildren,
}

let profile = {
    path: `/:locale${getLocaleRegex()}?/profile`,
    // path: '/example',
    //name: 'profile',
    component: Main,
    isHere: false,
    showMenu: true,
    classIcon: "icon iconpos-order",
    meta: {
        isPublic: true,
    },
    children: profileChildren,
}










const routes = [
    homePage,
    signUp,
    signIn,
    lospassword,
    aboutUs,
    profile,
    {
        path: "/:catchAll(.*)",
        //name: "NotFound",
        component: NotFound,
        meta: {
            requiresAuth: false
        }
    }
]

export default routes;
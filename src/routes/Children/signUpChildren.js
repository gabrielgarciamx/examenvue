import infoPage from "../../components/access/signUp.vue";

export default [{
    path: '',
    name: 'sign-up-main',
    showBreadcrumb: true,
    component: infoPage,
    meta: {
        isPublic: false,
        i18nPage: 'signUp',
    },
}, ]
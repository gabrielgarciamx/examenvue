import list from "../../components/Examples/list.vue";
import config from "../../components/Examples/config.vue";

export default [
	{
		path: 'list',
		name: 'list',
		showBreadcrumb: true,
		component: list
	},
	{
		path: '',
		name: 'config',
		showBreadcrumb: true,
		component: config
	}
]
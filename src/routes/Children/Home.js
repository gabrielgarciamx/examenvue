import home from "../../components/home.vue";

export default [
	{
		path: '',
		name: 'home',
		showBreadcrumb: true,
		component: home
	}
]
import infoPage from "../../components/access/losPassword.vue";

export default [{
    path: '',
    name: 'lost-password-main',
    showBreadcrumb: true,
    component: infoPage,
    meta: {
        isPublic: false,
        i18nPage: 'lost-password',
    },
}, ]
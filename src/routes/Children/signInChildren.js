import infoPage from "../../components/access/signIn.vue";

export default [{
    path: '',
    name: 'signin-main',
    showBreadcrumb: true,
    component: infoPage,
    meta: {
        isPublic: false,
        i18nPage: 'signin',
    },
}, ]
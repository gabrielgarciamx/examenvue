import index from "../../components/aboutUs/index.vue";


export default [
	{
		path: '',
		name: 'aboutUs-main',
		showBreadcrumb: true,
		component: index,
		meta:{
			isPublic: false,
			i18nPage: 'aboutUs', 
		},
	},
]
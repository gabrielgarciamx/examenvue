import index from "../../components/profile/index.vue";

export default [{
    path: '',
    name: 'profile-main',
    showBreadcrumb: true,
    component: index,
    meta: {
        isPublic: false,
        i18nPage: 'profile',
    },
}, ]
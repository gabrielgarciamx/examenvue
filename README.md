# Gabriel Garcia Examen

#### Requerimientos

node.js >= 12


#### Instalación

`npm install`

#### Ejecución de servidor de desarrollo

Ambiente de **desarrollo**
`npm run dev`



#### Generación de distribuible

Ambiente de **desarrollo**
`npm run build_dev`


#### Redes sociales


#### Git
https://bitbucket.org/gabrielgarciamx/

#### Repo
https://gabrielgarciamx@bitbucket.org/gabrielgarciamx/examenvue.git


#### WebSite
https://gabrielgarcia.mx/


#### Lindkedin
https://linkedin.com/in/gabgarbar



